
// Initialisation des classes du Générateur de vue
const OV = new OdeValidator();
const OL = new OdeLogger();
const ON = new GenerateurNavigation();

// Fonction de navigation entre les onglets 
function displayOnglet( onglet_id ){
    ON.route( onglet_id )
}

function addLigneRelation( _champ_name, _module_name, _type, _unique, _condition ){

    var nombre_ligne = $("#champ_"+_champ_name).children().length;
    if ( nombre_ligne < 4 ) {
        var ligne_html = '<div style="margin: 1% 0% 1% 30%;" class="col-md-8">';
        ligne_html +=    '<div id="display_value_champ_'+_champ_name+'_' + nombre_ligne + '" data-id-selected="" class="col-md-9" style="overflow: hidden; display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;border-radius: 3px; height: 47px; background: #c3e7fd none repeat scroll 0 0 !important; border: 1px solid #b8daef; font-size: 14px; font-weight: 400; color: #534d64; padding: 12px !important;"></div>';
        ligne_html +=    '<div class="col-md-3" style="text-align: center;">';
        ligne_html +=        '<span class="id-ff multiple">';
        ligne_html +=            '<button type="button" id="btn_statut_initial" title="Sélectionner" onclick="selectRelatedModule(\''+_champ_name + '_' + nombre_ligne +'\',\'' + _module_name + '\',\'' + _type + '\',\'' + _unique + '\',\'data-parent='+_champ_name+'\')" class="button firstChild" value="Sélectionner">';
        ligne_html +=                '<img src="themes/SuiteP/images/id-ff-select.png?v=9PUULME3DHwNvxEgZrpwig">';
        ligne_html +=            '</button>';
        ligne_html +=            '<button type="button" id="btn_clr_statut_initial" title="Clear Selection" onclick="deleteRelatedModule(\''+_champ_name + '_' + nombre_ligne +'\',\'' + _unique + '\',\'data-parent='+_champ_name+'\')" class="button lastChild" value="Supprimer la selection">';
        ligne_html +=                '<img src="themes/SuiteP/images/id-ff-clear.png?v=9PUULME3DHwNvxEgZrpwig">';
        ligne_html +=            '</button>';
        ligne_html +=        '</span>';
        ligne_html +=    '</div>';
        ligne_html +='</div>';
        $("#champ_"+_champ_name).append(ligne_html);
    }

}

// Fonction pour séléctionner un enregistrement ( Champs de type relation )
function selectRelatedModule(_champ_name,_module_name,_type,_unique,_condition){
    var width = 600;
    var height = 400;
    var json_data = {
        "call_back_function":"callBackFunction",
        "form_name":{
            "champ_name" :_champ_name,
            "module_name" :_module_name,
            "type" :_type,
            "unique" :_unique,
            "condition" :_condition,
        },
        "field_to_name_array":{"id":"id","name":"name"}
    };
    open_popup(_module_name, width, height, "", true, true, json_data , "", true );
}

// Fonction pour supprimer un enregistrement ( Champs de type relation )
function deleteRelatedModule( _champ_name, _type, _condition ){


    $( "#display_value_champ_" + _champ_name ).empty();

    var parent_champ_name =  ( _condition.indexOf('data-parent=') > -1 ) ? _condition.split('data-parent=')[1] : _champ_name;

    if ( _type === "multi" ) {

        var ids_selected = $("#value_champ_"+parent_champ_name).val();
        var id_to_delete =  $("#display_value_champ_"+ _champ_name).attr( "data-id-selected" );
        $("#value_champ_"+parent_champ_name).val( ids_selected.replace( id_to_delete, '') );
       
    } else {
        $( "#value_champ_" + _champ_name ).val("");
    } 

    $( "#display_value_champ_"+ _champ_name).attr( "data-id-selected", "")

}

// Fonction qui se déclenche quand on séléctionne un enregistrement ( Champs de type relation )
function callBackFunction(popup_reply_data) {
   

    var champ_params = popup_reply_data.form_name;
    var module_selected = popup_reply_data.name_to_value_array;
    var module_id = module_selected.id;
    var module_name = module_selected.name;
    var condition = champ_params.condition;
    var champ_name = champ_params.champ_name;
    var parent_champ_name =  ( condition.indexOf('data-parent=') > -1 ) ? condition.split('data-parent=')[1] : champ_name;
    var dossier_module_ids = $("#value_champ_"+parent_champ_name).val();

    if ( champ_params.unique !== "multi" || dossier_module_ids === "" ){

        $( "#value_champ_" + parent_champ_name).val(module_id);  
        $( "#display_value_champ_" + champ_name ).empty();
        $( "#display_value_champ_"+ champ_name).attr( "data-id-selected", module_id )
        $( "#display_value_champ_" + champ_name ).append( formatString(module_name) );

    } else {

        if ( dossier_module_ids.indexOf(module_id) === -1 ) {

            // Le cas ou on séléctionne un nouveau elu sur un autre existant
            var id_to_delete = ( $( "#display_value_champ_" + champ_name ).text() !== "" ) ? $( "#display_value_champ_"+ champ_name).attr("data-id-selected") : "";
   
            // L'élément séléctionné est deja présent sur le dossier => Il ne peut pas etre ajouter à nouveau
            $( "#value_champ_" + parent_champ_name).val( dossier_module_ids.replace( id_to_delete, '') + "|" + module_id );  
            $( "#display_value_champ_" + champ_name ).empty();
            $( "#display_value_champ_"+ champ_name).attr( "data-id-selected", module_id )
            $( "#display_value_champ_" + champ_name ).append( formatString(module_name) );
        }
        
    }

}

// Formatage des chaine de caracteres avec "'" qui s'affiche en code hexa
function formatString(string){
    var string_formated = "";
    if(isStringValid(string)){
       string_formated = string.replace(/&#039;/gi, "'");
    }
    return string_formated;
}

/**------------------------------------------------------------------------- Fonctions Ajax ------------------------------------------------------------------- */

// Cette variable regroupe les differentes fonctions des actions à mener apres la réponse de l'Ajax
var retourFunctionModifier = {
    editDossier: function(data) {
        if (data) {
            if (data['statut'] == "ok") {
                var redirect = "/index.php?module=OPS_dossier&return_module=OPS_dossier&action=DetailView&record="+data['data']['id'];
                window.location.replace(redirect);
                
            } else {
                var erreur = data['data']; 
                console.log("addOnglet() => Erreur =", erreur)
            }
        } else {
            console.log(" addOnglet() => format de retour différent de JSON ")
        }
    },
};
var retourFunction = (typeof retourFunction === 'undefined') ? retourFunctionModifier : Object.assign(retourFunction, retourFunctionModifier);

// Cette variable regroupe les differentes fonctions des actions en attendant de recevoir le retour de la requete à envoyer au PHP
var loadingFunctionModifier = {
    editDossier: function() {
        console.log("loadingFunction => editDossier")
    },
};
var loadingFunction = (typeof loadingFunction === 'undefined') ? loadingFunctionModifier : Object.assign(loadingFunction, loadingFunctionModifier);

// Cette variable regroupe les differentes fonctions de récupération des données à envoyer au PHP
var getDataFunctionModifier = {

    editDossier: function() {
        $(".div_button_onglet" ).removeClass('tab-error');
        
        var dossier_id =  getInputValue("dossier_id");
        var demandeur_type =  getInputValue("demandeur_type_choisi");
        var type_tiers = ( demandeur_type == "OPS_individu ") ? "Individu" : "Personne Morale";
        var data_formulaire = getDataFormulaire();
        var json = ( verificationDataFormulaire(data_formulaire) === true) ? JSON.stringify(data_formulaire) : false;
        var data = ( isJson(json) && isStringValid(dossier_id) && isStringValid(type_tiers)) ? "json=" + utf8_to_b64(json) +"&dossier_id=" + dossier_id + "&type_tiers=" + type_tiers : false;
        return data;
    }

};
var getDataFunction = (typeof getDataFunction === 'undefined') ? getDataFunctionModifier : Object.assign(getDataFunction, getDataFunctionModifier);

// Fonction qui retourne tous les champs du formulaire ainsi que leurs valeurs
function getDataFormulaire(){
    var elements = {};
    var champs_formulaire = $('[id^="value_champ_"]');
    if( champs_formulaire.length > 0 ){
        for (let index = 0; index < champs_formulaire.length; index++) {
            const element = champs_formulaire[index];
            const id = ( element.id && isStringValid(element.id) && element.id.indexOf('value_champ_') > -1 ) ? element.id.replace(/value_champ_/g,'') : false;
            if( element.type === "checkbox" ){
                var value = ( element.checked && element.checked === true ) ? '1' : '0';
            } else if ( element.type === "select-multiple"){
                var value = getValueListeChoixMulti(id); 
            }else{
               // var value = ( element.value ) ? encodeURIComponent(element.value.replace(/\n/gi, " ")) : "";
               var value = document.getElementById(element.id).value;
            }
            if( id !== false ) elements[id] = value;
        }
    }
    return elements;
}

function getValueListeChoixMulti(id) {
    if ( $('#value_champ_'+id).val() ) {
        var value_select_filtred = $('#value_champ_'+id).val().filter(function (el) { 
            return ( el !== null && el !== undefined && el !== "") ? el : false;
        }); 
    }
    return ( value_select_filtred !== undefined && value_select_filtred.length > 0 ) ? "^" + value_select_filtred.join('^,^') + "^" : "";
}

// Fonction qui boucle sur les champs du formulaire pour vérifier leurs validités
function verificationDataFormulaire(data_formulaire){

    var create = true;
    if( Object.size(data_formulaire) > 0 ){
        Object.keys(data_formulaire).forEach(function(id) {
            var champ_id = ( isStringValid(id) ) ? id : false;
            var element = ( champ_id !== false ) ? document.getElementById("value_champ_"+champ_id) : false;
            if( element !== false ){
                var is_valid = OV.isValid(element);
                if( is_valid === false ) create = false;
            }
        });
    }else{
        create = false;
        console.log(" Formulaire vide ! ")
    }

    return create;
}

// Fonction qui corrige les champs de type pourcentage pour que la valeur soit toujours entre 0 et 100
function isPourcentageValid(_this){
    var value = (_this.value) ? _this.value : 0;
    if( value > 100 ) _this.value = 100 ;
    if( value < 0 ) _this.value = 0 ;
}

/* **************** Gestion du calcul d'aide **************** */

$( document ).ready( function() {

    if( isChampsCalculAideExist() ) initCalculAide();
    if( isChampExist("value_champ_decision_commission") && isChampExist("value_champ_montant_propose") && isChampExist("value_champ_montant_vote")   ) $("#value_champ_decision_commission").on( "change", addMontantVote ); 


});



function isChampExist(id_champ){

    console.group("Vérification de l'existance d'un champ : "+id_champ);
    var valid = true;

    if (id_champ) {
        if ( $("#"+id_champ).length ) {
            console.log("%c Champ "+id_champ.replace(/value_champ_/g, "")+" : %c OK ", "", "font-weight: bold;color: #1E8449;");
        } else {
            valid = false;
            console.log("%c Champ "+id_champ.replace(/value_champ_/g, "")+" : %c FAIL ", "", "font-weight: bold;color: #D35400;");
        }
    }

    if ( valid === true ) {
        console.log("%c ➥Champ existant ! ", "font-weight: bold;color: #1E8449;");
    } else {
        console.log("%c ➥Champ inexistant ! ", "font-weight: bold;color: #D35400;");
    }
    console.groupEnd();

    return valid;

}


function isChampsCalculAideExist(){

    console.group("Vérification champs Calcul de l'aide");
    var valid = true;

    var champs_calcul = [
        "value_champ_montant_total_depenses_previsionnel",
        "value_champ_montant_total_depenses_realise",
        "value_champ_montant_depenses_sub_previsionnel",
        "value_champ_montant_depenses_sub_realise",
        "value_champ_taux_prise_charge_dispositif_previsionnel",
        "value_champ_taux_prise_charge_dispositif_realise",
        "value_champ_montant_sub_previsionnel",
        "value_champ_montant_sub_realise",
        "value_champ_taux_financement_previsionnel",
        "value_champ_taux_financement_realise",
        "value_champ_total_financement_previsionnel",
        "value_champ_total_financement_realise",
        "value_champ_taux_financement_total_previsionnel",
        "value_champ_taux_financement_total_realise",
        "value_champ_total_cofinancement_previsionnel",
        "value_champ_total_cofinancement_realise",
    ];

    champs_calcul.forEach(function(champ_id){
        if (champ_id) {
            if ( $("#"+champ_id).length ) {
                console.log("%c Champ "+champ_id.replace(/value_champ_/g, "")+" : %c OK ", "", "font-weight: bold;color: #1E8449;");
            } else {
                valid = false;
                console.log("%c Champ "+champ_id.replace(/value_champ_/g, "")+" : %c FAIL ", "", "font-weight: bold;color: #D35400;");
            }
        }
    });

    if ( valid === true ) {
        console.log("%c ➥ Calcul d'aide initialisé ! ", "font-weight: bold;color: #1E8449;");
    } else {
        console.log("%c ➥ Calcul d'aide non initialisé ! ", "font-weight: bold;color: #D35400;");
    }

    console.groupEnd();
    return valid;

}

function initCalculAide(){

    // Initialisation de l'onglet CALCUL DE L'AIDE 
    $("#value_champ_montant_total_depenses_previsionnel").on( "blur", B1_updated ); // B1
    $("#value_champ_montant_depenses_sub_previsionnel").on( "blur", B2_updated ); // B2
    $("#value_champ_montant_sub_previsionnel").on( "blur", B4_updated ); // B4
    $("#value_champ_total_financement_previsionnel").on( "blur", B6_updated ); // B6
    $("#value_champ_montant_total_depenses_realise").on( "blur", C1_updated ); // C1
    $("#value_champ_montant_depenses_sub_realise").on( "blur", C2_updated ); // C2
    $("#value_champ_montant_sub_realise").on( "blur", C4_updated ); // C4
    $("#value_champ_total_financement_realise").on( "blur", C6_updated ); // C6

}


function addMontantVote(){

    if( value_champ_decision_commission.selectedOptions.valueOf()[0].value == "accord" ){
        $("#value_champ_montant_vote").val( $("#value_champ_montant_propose").val() );
    }

}

function addMontantPropose(){

    if( isChampExist( "value_champ_montant_propose" ) &&  isChampExist( "value_champ_montant_sub_previsionnel" )  ){

        $("#value_champ_montant_propose").val( $("#value_champ_montant_sub_previsionnel").val() );
    }

}

/**
 * Fonction qui met à jour les autres champs quand la valeur B1 est modifié
 * 
 */
function B1_updated(){
    calcul_B5();
    calcul_B7();
}

/**
 * Fonction qui met à jour les autres champs quand la valeur B2 est modifié
 * 
 */
function B2_updated(){
    calcul_B3();
}

/**
 * Fonction qui met à jour les autres champs quand la valeur B4 est modifié
 * 
 */
function B4_updated(){
    calcul_B3();
    calcul_B5();
    calcul_B8();
    addMontantPropose();
}

/**
 * Fonction qui met à jour les autres champs quand la valeur B6 est modifié
 * 
 */
function B6_updated(){
    calcul_B7();
    calcul_B8();
}

/**
 * Fonction qui met à jour les autres champs quand la valeur C1 est modifié
 * 
 */
function C1_updated(){
    calcul_C5();
    calcul_C7();
}

/**
 * Fonction qui met à jour les autres champs quand la valeur C2 est modifié
 * 
 */
function C2_updated(){
    calcul_C3();
}

/**
 * Fonction qui met à jour les autres champs quand la valeur C4 est modifié
 * 
 */
function C4_updated(){
    calcul_C3();
    calcul_C5();
    calcul_C8();
}

/**
 * Fonction qui met à jour les autres champs quand la valeur C6 est modifié
 * 
 */
function C6_updated(){
    calcul_C7();
    calcul_C8();
}

function getFloatVal( element ){
    return ( element.length > 0 ) ? parseFloat( element.val() ) : 0;
}

/**
 * Fonction qui calcul la valeur du champ B3
 * Formule :      B3 = ( 100 * B4 ) / B2
 */
function calcul_B3(){

    var B2 = $("#value_champ_montant_depenses_sub_previsionnel");
    var B3 = $("#value_champ_taux_prise_charge_dispositif_previsionnel");
    var B4 = $("#value_champ_montant_sub_previsionnel");

    if( B2.length > 0 && B3.length > 0 && B4.length > 0 ){
        if( getFloatVal(B2) > 0 ){
            var B3_val = ( 100 * getFloatVal(B4) ) / getFloatVal(B2);    
            B3.val( B3_val.toFixed(2) );
        }
    }

}

/**
 * Fonction qui calcul la valeur du champ C3
 * Formule :      C3 = ( 100 * C4 ) / C2
 */
function calcul_C3(){
    
    var C2 = $("#value_champ_montant_depenses_sub_realise");
    var C3 = $("#value_champ_taux_prise_charge_dispositif_realise");
    var C4 = $("#value_champ_montant_sub_realise");

    if( C2.length > 0 && C3.length > 0 && C4.length > 0 ){
        if( getFloatVal(C2) > 0 ){
            var C3_val =  ( 100 * getFloatVal(C4) ) / getFloatVal(C2);
            C3.val( C3_val.toFixed(2) );
        }
    }

}

/**
 * Fonction qui calcul la valeur du champ B5
 * Formule :      B5 = ( B4 / B1 ) * 100
 */
function calcul_B5(){

    var B1 = $("#value_champ_montant_total_depenses_previsionnel");
    var B4 = $("#value_champ_montant_sub_previsionnel");
    var B5 = $("#value_champ_taux_financement_previsionnel");

    if( B1.length > 0 && B4.length > 0 && B5.length > 0 ){
        if( getFloatVal(B1) > 0 ){
            var B5_val = (  getFloatVal(B4) / getFloatVal(B1) ) * 100 ; 
            B5.val( B5_val.toFixed(2) );
        }
    }

}

/**
 * Fonction qui calcul la valeur du champ C5
 * Formule :      C5 = ( C4 / C1 ) * 100
 */
function calcul_C5(){

    var C1 = $("#value_champ_montant_total_depenses_realise");
    var C4 = $("#value_champ_montant_sub_realise");
    var C5 = $("#value_champ_taux_financement_realise");

    if( C1.length > 0 && C4.length > 0 && C5.length > 0 ){
        if( getFloatVal(C1) > 0 ){
            var C5_val = ( getFloatVal(C4) / getFloatVal(C1) ) * 100;
            C5.val( C5_val.toFixed(2) );
        }
    }

}

/**
 * Fonction qui calcul la valeur du champ B7
 * Formule :      B7 = ( B6 / B1 ) * 100
 */
function calcul_B7(){

    var B1 = $("#value_champ_montant_total_depenses_previsionnel");
    var B6 = $("#value_champ_total_financement_previsionnel");
    var B7 = $("#value_champ_taux_financement_total_previsionnel");

    if( B1.length > 0 && B6.length > 0 && B7.length > 0 ){
        if( getFloatVal(B1) > 0 ){
            var B7_val = (  getFloatVal(B6) /  getFloatVal(B1) ) * 100 ;
            B7.val( B7_val.toFixed(2) );
        }
    }

}

/**
 * Fonction qui calcul la valeur du champ C7
 * Formule :      C7 = ( C6 / C1 ) * 100
 */
function calcul_C7(){
    
    var C1 = $("#value_champ_montant_total_depenses_realise");
    var C6 = $("#value_champ_total_financement_realise");
    var C7 = $("#value_champ_taux_financement_total_realise");

    if( C1.length > 0 && C6.length > 0 && C7.length > 0 ){
        if( getFloatVal(C1) > 0 ){
            var C7_val = (  getFloatVal(C6) /  getFloatVal(C1) ) * 100;
            C7.val( C7_val.toFixed(2) );
        }
    }

}

/**
 * Fonction qui calcul la valeur du champ B8
 * Formule :      B8 =  B6 - B4
 */
function calcul_B8(){

    var B4 = $("#value_champ_montant_sub_previsionnel");
    var B6 = $("#value_champ_total_financement_previsionnel");
    var B8 = $("#value_champ_total_cofinancement_previsionnel");

    if( B4.length > 0 && B6.length > 0 && B8.length > 0 ){
        var B8_val = getFloatVal(B6) - getFloatVal(B4);
        B8.val(  B8_val.toFixed(2) );
    }

}

/**
 * Fonction qui calcul la valeur du champ B8
 * Formule :      C8 =  C6 - C4
 */
function calcul_C8(){

    var C4 = $("#value_champ_montant_sub_realise");
    var C6 = $("#value_champ_total_financement_realise");
    var C8 = $("#value_champ_total_cofinancement_realise");

    if( C4.length > 0 && C6.length > 0 && C8.length > 0 ){
        var C8_val = getFloatVal(C6) - getFloatVal(C4);
        C8.val(  C8_val.toFixed(2) );
    }

}

/* ********************************************************** */

