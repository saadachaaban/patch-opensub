var date_pickers = [];

const OV = new OdeValidator();
const OL = new OdeLogger();
const ODE_LOADING = new OdeLoading();

String.prototype.utf8_to_b64=function(){return $.base64.encode(unescape(encodeURIComponent(this)))};
String.prototype.b64_to_utf8=function(){return decodeURIComponent(escape($.base64.decode(this)))};
/**/

/** Returns a properly escaped Unicode16 string from a base64 decoded to use with INPUT Element setteable . **/
String.prototype.eub64_to_val = function() {return unescape(atob(btoa(this)))};



function initDatePickers(){
    for (let index = 0; index < date_pickers.length; index++) {
        const element = date_pickers[index];
         $( "#" + element ).datepicker({
            altField: "#datepicker",
            closeText: 'Fermer',
            firstDay: 1 ,
            dateFormat: 'dd/mm/yy'
            });
        $.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
    }
}

function choixDispositif( dispositif_id, dispositif_name ){
    if( isStringValid(dispositif_id) && isStringValid(dispositif_name)){
        if( document.getElementById("erreur_"+dispositif_id) ){
            var libelle_erreur = document.getElementById("erreur_"+dispositif_id).getAttribute("title");
            viderElement("div_libelle_erreur");
            document.getElementById("div_libelle_erreur").appendChild(document.createTextNode(libelle_erreur));
            changeElementDisplay("div_libelle_erreur","block");
            setTimeout(function(){
                changeElementDisplay("div_libelle_erreur","none");
            }, 7000);
        }else{
            setInputValue("dispositif_choisi_id",dispositif_id)
            setInputValue("dispositif_choisi_name",dispositif_name)
            changeElementDisplay("div_choix_dispositif","none");
            changeElementDisplay("div_choix_demandeur","block");
            if( $_GET("type_tiers") != null ){
                setInputValue("demandeur_type_choisi",$_GET("type_tiers"));
                changeElementDisplay("div_choix_demandeur","none");
                execute('initForm','OPS_dossier');
            }
            else{
                changeElementDisplay("div_choix_demandeur","block");
            }
        }
    }
}

function choixDemandeur(demandeur_type){
    if(isStringValid(demandeur_type)){
        setInputValue("demandeur_type_choisi",demandeur_type)
        changeElementDisplay("div_choix_demandeur","none");
        execute('initForm','OPS_dossier');
    }
}

function displayDossierForm(data){

    var total = Object.size(data['onglets']);

    var liste_thematique = (data['params']['liste_thematique']) ? data['params']['liste_thematique'] : "";
    setInputValue("liste_thematique",liste_thematique)
    var count = 0;
    Object.keys(data['onglets']).map(function(index, value) {
        var onglet = data['onglets'][index];
        genereBoutonOnglet( "div_boutons_onglets", onglet, count, total );
        genereLayoutOnglet( "div_layouts_onglets", onglet, count, total );
        count++;
    });

    changeElementDisplay("div_dossier","block");
}

function addLigneRelation( _champ_name, _module_name, _type, _unique, _condition ){

    var nombre_ligne = $("#champ_"+_champ_name).children().length;
    if ( nombre_ligne < 4 ) {
        var ligne_html = '<div style="margin: 1% 0% 1% 30%;" class="col-md-8">';
        ligne_html +=    '<div id="display_value_champ_'+_champ_name+'_' + nombre_ligne + '" data-id-selected="" class="col-md-9" style="overflow: hidden; display: -webkit-box;-webkit-line-clamp: 3;-webkit-box-orient: vertical;border-radius: 3px; height: 47px; background: #c3e7fd none repeat scroll 0 0 !important; border: 1px solid #b8daef; font-size: 14px; font-weight: 400; color: #534d64; padding: 12px !important;"></div>';
        ligne_html +=    '<div class="col-md-3" style="text-align: center;">';
        ligne_html +=        '<span class="id-ff multiple">';
        ligne_html +=            '<button type="button" id="btn_statut_initial" title="Sélectionner" onclick="selectRelatedModule(\''+_champ_name + '_' + nombre_ligne +'\',\'' + _module_name + '\',\'' + _type + '\',\'' + _unique + '\',\'data-parent='+_champ_name+'\')" class="button firstChild" value="Sélectionner">';
        ligne_html +=                '<img src="themes/SuiteP/images/id-ff-select.png?v=9PUULME3DHwNvxEgZrpwig">';
        ligne_html +=            '</button>';
        ligne_html +=            '<button type="button" id="btn_clr_statut_initial" title="Clear Selection" onclick="deleteRelatedModule(\''+_champ_name + '_' + nombre_ligne +'\',\'' + _unique + '\',\'data-parent='+_champ_name+'\')" class="button lastChild" value="Supprimer la selection">';
        ligne_html +=                '<img src="themes/SuiteP/images/id-ff-clear.png?v=9PUULME3DHwNvxEgZrpwig">';
        ligne_html +=            '</button>';
        ligne_html +=        '</span>';
        ligne_html +=    '</div>';
        ligne_html +='</div>';
        $("#champ_"+_champ_name).append(ligne_html);
    }

}

function genereLayoutOnglet(parent_id , onglet, count, total){

    var display_layout = ( count == 0 ) ? "block" : "none";

    var onglet_id = onglet.id;
    var onglet_champs = onglet.champs;

    var layout_html = '<div id="layout_onglet_' + onglet_id + '" class="div_layout_onglet" style="display: ' + display_layout + ';">';

    Object.keys(onglet_champs).map(function(index) {

        var ligne = onglet_champs[index];
        layout_html += '<div class="row" style="background: white; margin: 0px; padding: 5px;">';

        Object.keys(ligne).map(function(index) {

            var champ = ligne[index];
           
            if( isStringValid(champ.name) && isStringValid(champ.libelle) ){

                layout_html += '<div id="champ_' + champ.name + '" class="col-md-6 champ_referentiel champ_panel" ';
                layout_html += ( champ.type != "relation" ) ?  'style="padding-right: 4%;">' : '>';
                layout_html +=      '<div id="titre_champ_' + champ.name + '" class="col-md-4" style="white-space: initial; padding-top: 15px;" ';
                layout_html +=      ( isStringValid(champ.aide) ) ? 'title="' + champ.aide + '"' : '' ;
                layout_html +=      '>';
                layout_html +=          champ.libelle.replace(/&#039;/gi, "'") + ":" ;
                layout_html +=          ( champ.obligatoire == 1 ) ? '<span style="color:red;" >*</span>' : '' ;
                layout_html +=          ( isStringValid(champ.aide) ) ? '<i class="fa fa-info-circle" style="color: #35B3C5; margin-left: 3px;" ></i>' : '' ;
                if ( champByType.hasOwnProperty(champ.type) && champ.type === "relation" ) {
                    var params = isReponseValid( b64_to_utf8(champ.params) );
                    if ( params.unique === "multi" ){
                        layout_html += '<i class="fa fa-plus" style="background: #35B3C5;color: white;border-radius: 16px;padding: 4px;margin-left: 9px;cursor: pointer;" onclick="addLigneRelation(\''+champ.name+'\',\''+params.module_name+'\',\''+params.type+'\',\''+params.unique+'\',\''+params.condition+'\')"></i>';
                    }
                }
                layout_html += 
                '<small style="color:red;display:none;" >Message d\'erreur</small>' ;
                layout_html += '</div>';
                layout_html += ( isCustomField(champ.name) ) ? getCustomField(champ) : getBasicField(champ);
                layout_html += '</div>';
                
            }else{
                layout_html += '<div class="col-md-6 champ_referentiel champ_panel" style="padding-right: 4%;" >';
                layout_html += '</div>';
            }
        });
        layout_html += '</div>';
    });

    layout_html += '</div>';
    $("#"+parent_id).append(layout_html);

    initDatePickers();  

}


function getBasicField(champ){
    return ( champByType.hasOwnProperty(champ.type) ) ? champByType[champ.type](champ) : getInputChamp(champ);
}

function getCustomField(champ){
    return ( customChamps.hasOwnProperty(champ.name) ) ? customChamps[champ.name](champ) : getInputChamp(champ);
}


var champByType = {
  
    text: function(champ) {
        if (champ) {

            var champ_html = '<input type="text" class="ode_input_hidden col-md-8"';
            champ_html +=    ( champ.ineditable == 1 ) ? 'style="display: block; background:#f8f8f8 !important;"' : 'style="display: block;"';
            champ_html +=    ( champ.obligatoire == 1 ) ? ' required ' : '';
            champ_html +=    ( champ.ineditable == 1 ) ? ' disabled ' : '';
            champ_html +=    ( isStringValid(champ.defaut) ) ? 'value="' + champ.defaut + '"' : '';
            champ_html +=    'name="' + champ.name + '"';
            champ_html +=    'id="value_champ_' + champ.name + '" >';
            return champ_html;

        } else {
            return getDivErreur();
        }
    },

    montant: function(champ) {
        if (champ) {

            var champ_html = '<input type="number" class="ode_input_hidden col-md-8" min="0" step=".01"';
            champ_html +=    ( champ.ineditable == 1 ) ? 'style="display: block; background:#f8f8f8 !important;"' : 'style="display: block;"';
            champ_html +=    ( champ.obligatoire == 1 ) ? ' required ' : '';
            champ_html +=    ( champ.ineditable == 1 ) ? ' disabled ' : '';
            champ_html +=    ( isStringValid(champ.defaut) ) ? 'value="' + champ.defaut + '"' : '';
            champ_html +=    'name="' + champ.name + '"';
            champ_html +=    'id="value_champ_' + champ.name + '" >';
            return champ_html;

        } else {
            return getDivErreur();
        }
    },

    liste: function(champ) {
        var listes = getListes();
        if ( champ && listes ) {
            var params = isReponseValid( b64_to_utf8( champ.params ) );
            if( params ){
                var liste = ( listes.hasOwnProperty(params.liste_name) ) ? listes[params.liste_name] : false;
                if (liste) {
                    var champ_html = '<select class="col-md-8" ';
                    champ_html +=    ( champ.obligatoire == 1 ) ? ' required ' : '';
                    champ_html +=    ( champ.ineditable == 1 ) ? ' disabled ' : '';
                    champ_html +=    'name="' + champ.name + '"';
                    champ_html +=    'id="value_champ_' + champ.name + '" >';
                    Object.keys(liste).map(function(option_id) {
                        var option_name = liste[option_id];
                        var is_selected =  ( isStringValid(champ.defaut) && champ.defaut === option_id ) ? ' selected ' : '';
                        champ_html += '<option ' + is_selected + ' value="' + option_id + '">' + option_name + '</option>';
                    });
                    champ_html += '</select>';
                    return champ_html;
                }
            }
        } else {
            return getDivErreur();
        }
    },

    relation: function(champ) {
        var params = isReponseValid( b64_to_utf8(champ.params) );
        if (champ) {
            var champ_html = '<div style="margin-left: -3%;" class="col-md-8">';
            champ_html +=       '<div class="col-md-9" style="border-radius: 3px; height: 47px; background: #c3e7fd none repeat scroll 0 0 !important; border: 1px solid #b8daef; font-size: 14px; font-weight: 400; color: #534d64; padding: 12px!important;"'; 
            champ_html +=       'id="display_value_champ_' + champ.name + '" data-id-selected="" ></div>';
            champ_html +=       '<div class="col-md-3" style="text-align: center;">';
            champ_html +=           '<span class="id-ff multiple">';
            champ_html +=               '<button type="button" id="btn_statut_initial" title="Sélectionner" class="button firstChild" value="Sélectionner"';
            champ_html +=               ( champ.ineditable == 1 ) ? ' disabled ' : '';
            champ_html +=               'onclick="selectRelatedModule(\'' + champ.name + '\',\'' + params.module_name + '\',\'' + params.type + '\',\'' + params.unique + '\',\'' + params.condition + '\')" >';
            champ_html +=                   '<img src="themes/SuiteP/images/id-ff-select.png?v=9PUULME3DHwNvxEgZrpwig">';
            champ_html +=               '</button>';
            champ_html +=               '<button type="button" id="btn_clr_statut_initial" title="Clear Selection" class="button lastChild"  value="Supprimer la selection"';
            champ_html +=               ( champ.ineditable == 1 ) ? ' disabled ' : '';
            champ_html +=               'onclick="deleteRelatedModule(\'' + champ.name + '\',\'' + params.unique + '\',\'' + params.condition + '\')">';
            champ_html +=                   '<img src="themes/SuiteP/images/id-ff-clear.png?v=9PUULME3DHwNvxEgZrpwig">';
            champ_html +=               '</button>';
            champ_html +=           '</span>';
            champ_html +=       '</div>';
            champ_html +=       '<input name="' + champ.name + '" type="hidden"';
            champ_html +=           ( champ.obligatoire == 1 ) ? ' required ' : '';
            champ_html +=       'id="value_champ_' + champ.name + '">';
            champ_html +=   '</div>';
            return champ_html;
          } else {
            return getDivErreur();
        }
    },

    date: function(champ) {
        if (champ) {
            var champ_html = '<input type="text" class="ode_input_hidden col-md-6" autocomplete="off" ';
            champ_html +=    ( champ.ineditable == 1 ) ? 'style="display: block; background:#f8f8f8 !important;"' : 'style="display: block;"';
            champ_html +=    ( champ.obligatoire == 1 ) ? ' required ' : '';
            champ_html +=    ( champ.ineditable == 1 ) ? ' disabled ' : '';
            champ_html +=    'value="' + getDefaultDate( champ.defaut ) + '"';
            champ_html +=    'name="' + champ.libelle + '"';
            champ_html +=    'id="value_champ_' + champ.name + '" >';
            date_pickers.push("value_champ_"+champ.name);
            return champ_html;
        } else {
            return getDivErreur();
        }
    },

    lien: function(champ) {
        if (champ) {
            var champ_html = '<input type="url" class="ode_input_hidden col-md-6"';
            champ_html +=    ( champ.ineditable == 1 ) ? 'style="display: block; background:#f8f8f8 !important;"' : 'style="display: block;"';
            champ_html +=    ( champ.obligatoire == 1 ) ? ' required ' : '';
            champ_html +=    ( champ.ineditable == 1 ) ? ' disabled ' : '';
            champ_html +=    ( isStringValid(champ.defaut) ) ? 'value="' + champ.defaut + '"' : '';
            champ_html +=    'name="' + champ.libelle + '"';
            champ_html +=    'id="value_champ_' + champ.name + '" >';
            return champ_html;
        } else {
            return getDivErreur();
        }
    },

    text_long: function(champ) {
        if (champ) {
            var champ_html = '<textarea class="ode_input_hidden" style="display: block;height: 11em;resize: none;overflow-y: scroll;';
            champ_html +=    ( champ.ineditable == 1 ) ? 'background: #f8f8f8 !important; "' : '"';
            champ_html +=    ( champ.obligatoire == 1 ) ? ' required ' : '';
            champ_html +=    ( champ.ineditable == 1 ) ? ' disabled ' : '';
            champ_html +=    'name="' + champ.libelle + '"';
            champ_html +=    'id="value_champ_' + champ.name + '" >';
            champ_html +=    ( isStringValid(champ.defaut) ) ? champ.defaut  : '';
            champ_html +=    '</textarea>';
            return champ_html;
        } else {
            return getDivErreur();
        }
    },

    checkbox: function(champ) {
        if (champ) {
            var champ_html = '<div class="col-md-8" style="border-radius: 3px;height: 47px;background: #c3e7fd none repeat scroll 0 0 !important;border: 1px solid #b8daef;font-size: 14px;font-weight: 400;color: #534d64;padding: 14px!important;" >';
            champ_html += '<input type="checkbox" ';
            champ_html +=    ( champ.ineditable == 1 ) ? 'style="display: block; background:#f8f8f8 !important;"' : 'style="display: block;"';
            champ_html +=    ( champ.obligatoire == 1 ) ? ' required ' : '';
            champ_html +=    ( champ.ineditable == 1 ) ? ' disabled ' : '';
            champ_html +=    ( champ.defaut == "1" ) ? ' checked ' : '';
            champ_html +=    'name="' + champ.libelle + '"';
            champ_html +=    'id="value_champ_' + champ.name + '" >';
            champ_html += '</div>';
            return champ_html;
        } else {
            return getDivErreur();
        }
    },

    pourcentage: function(champ) {
        if (champ) {
            var champ_html = '<input type="number" class="ode_input_hidden" min="0" max="100" step=".01" onchange="isPourcentageValid(this)" ';
            champ_html +=    ( champ.ineditable == 1 ) ? 'style="display: block; background:#f8f8f8 !important;"' : 'style="display: block;"';
            champ_html +=    ( champ.obligatoire == 1 ) ? ' required ' : '';
            champ_html +=    ( champ.ineditable == 1 ) ? ' disabled ' : '';
            champ_html +=    ( isStringValid(champ.defaut) ) ? 'value="' + champ.defaut + '"' : '';
            champ_html +=    'name="' + champ.name + '"';
            champ_html +=    'id="value_champ_' + champ.name + '" >';
            return champ_html;
        } else {
            return getDivErreur();
        }
    },

    liste_choix_simple: function(champ) {
        var params = isReponseValid( b64_to_utf8( champ.params ) );
        if( params.liste ){
            var liste = params.liste;
            if (liste) {
                var champ_html = '<select class="col-md-8" ';
                champ_html +=    ( champ.obligatoire == 1 ) ? ' required ' : '';
                champ_html +=    ( champ.ineditable == 1 ) ? ' disabled ' : '';
                champ_html +=    'name="' + champ.name + '"';
                champ_html +=    'id="value_champ_' + champ.name + '" >';
                champ_html +=       '<option value=""></option>';
                Object.keys(liste).map(function(option_id) {
                    var option_name = liste[option_id];
                    var is_selected =  ( isStringValid(champ.defaut) && champ.defaut === option_id ) ? ' selected ' : '';
                    champ_html += '<option ' + is_selected + ' value="' + option_id + '">' + option_name + '</option>';
                });
                champ_html += '</select>';
                return champ_html;
            } else {
                return getDivErreur();
            }
        }else{
            return getDivErreur();
        }

    },
    liste_choix_multi: function(champ) {
        var params = isReponseValid( b64_to_utf8( champ.params ) );
        if( params.liste ){
            var liste = params.liste;
            if (liste) {
                var champ_html = '<select multiple class="col-md-8" ';
                champ_html +=    ( champ.obligatoire == 1 ) ? ' required ' : '';
                champ_html +=    ( champ.ineditable == 1 ) ? ' disabled ' : '';
                champ_html +=    'name="' + champ.name + '"';
                champ_html +=    'id="value_champ_' + champ.name + '" >';
                champ_html +=       '<option value=""></option>';
                Object.keys(liste).map(function(option_id) {
                    var option_name = liste[option_id];
                    var is_selected =  ( isStringValid(champ.defaut) && champ.defaut === option_id ) ? ' selected ' : '';
                    champ_html += '<option ' + is_selected + ' value="' + option_id + '">' + option_name + '</option>';
                });
                champ_html += '</select>';
                return champ_html;
            } else {
                return getDivErreur();
            }
        }else{
            return getDivErreur();
        }

    },
    
};

function getDefaultDate( defaut_value ){

    var defauts = {
        "-1 day": { day:-1, month:0, year:0 },
        "now": { day:0, month:0, year:0 },
        "+1 day": { day:1, month:0, year:0 },
        "+1 week": { day:7, month:0, year:0 },
        "next monday": { day:0, month:0, year:0 }, 
        "next friday": { day:0, month:0, year:0 }, 
        "+2 weeks": { day:14, month:0, year:0 },
        "+1 month": { day:0, month:1, year:0 },
        "first day of next month": { day:0, month:0, year:0 }, 
        "+3 months": { day:0, month:3, year:0 }, 
        "+6 months": { day:0, month:6, year:0 },
        "+1 year": { day:0, month:0, year:1 }
    }

    if ( defauts.hasOwnProperty( defaut_value ) ){

        var currentTime = new Date();

        if( defaut_value === "next monday" ){
            currentTime.setDate( currentTime.getDate() + ( 1 - 1 - currentTime.getDay() + 7) % 7 + 1 );
        } else if( defaut_value === "next friday" ){
            currentTime.setDate( currentTime.getDate() + ( 5 - 1 - currentTime.getDay() + 7) % 7 + 1 );
        } else if( defaut_value === "first day of next month" ){
            if ( currentTime.getMonth() == 11 ) {
                currentTime = new Date(currentTime.getFullYear() + 1, 0, 1);
            } else {
                currentTime = new Date(currentTime.getFullYear(), currentTime.getMonth() + 1, 1);
            }
        }else{
            currentTime.setDate( currentTime.getDate() + defauts[defaut_value].day );
            currentTime.setMonth( currentTime.getMonth() + defauts[defaut_value].month );
            currentTime.setFullYear( currentTime.getFullYear() + defauts[defaut_value].year );
        }

        var dd = currentTime.getDate();
        var mm = currentTime.getMonth() + 1;
        var yyyy = currentTime.getFullYear();

        if(dd<10) dd='0'+dd;
        if(mm<10) mm='0'+mm;

        return (dd+"/"+mm+"/"+yyyy);
    }else{
        return "";
    }

}

function isCustomField(_name){
    return ( customChamps.hasOwnProperty(_name) ) ? true : false;
}

function selectRelatedModule(_champ_name,_module_name,_type,_unique,_condition){
        var width = 600;
        var height = 400;
        var json_data = {
            "call_back_function":"callBackFunction",
            "form_name":{
                "champ_name" :_champ_name,
                "module_name" :_module_name,
                "type" :_type,
                "unique" :_unique,
                "condition" :_condition,
            },
            "field_to_name_array":{"id":"id","name":"name"}
        };
        open_popup(_module_name, width, height, "", true, true, json_data , "", true );
}

function deleteRelatedModule( _champ_name, _type, _condition ){


    $( "#display_value_champ_" + _champ_name ).empty();

    var parent_champ_name =  ( _condition.indexOf('data-parent=') > -1 ) ? _condition.split('data-parent=')[1] : _champ_name;

    if ( _type === "multi" ) {

        var ids_selected = $("#value_champ_"+parent_champ_name).val();
        var id_to_delete =  $("#display_value_champ_"+ _champ_name).attr( "data-id-selected" );
        $("#value_champ_"+parent_champ_name).val( ids_selected.replace( id_to_delete, '') );
       
    } else {
        $( "#value_champ_" + _champ_name ).val("");
    } 

    $( "#display_value_champ_"+ _champ_name).attr( "data-id-selected", "")

}

function callBackFunction(popup_reply_data) {
   

    var champ_params = popup_reply_data.form_name;
    var module_selected = popup_reply_data.name_to_value_array;
    var module_id = module_selected.id;
    var module_name = module_selected.name;
    var condition = champ_params.condition;
    var champ_name = champ_params.champ_name;
    var parent_champ_name =  ( condition.indexOf('data-parent=') > -1 ) ? condition.split('data-parent=')[1] : champ_name;
    var dossier_module_ids = $("#value_champ_"+parent_champ_name).val();

    if ( champ_params.unique !== "multi" || dossier_module_ids === "" ){

        $( "#value_champ_" + parent_champ_name).val(module_id);  
        $( "#display_value_champ_" + champ_name ).empty();
        $( "#display_value_champ_"+ champ_name).attr( "data-id-selected", module_id )
        $( "#display_value_champ_" + champ_name ).append( formatString(module_name) );

    } else {

        if ( dossier_module_ids.indexOf(module_id) === -1 ) {

            // Le cas ou on séléctionne un nouveau elu sur un autre existant
            var id_to_delete = ( $( "#display_value_champ_" + champ_name ).text() !== "" ) ? $( "#display_value_champ_"+ champ_name).attr("data-id-selected") : "";
   
            // L'élément séléctionné est deja présent sur le dossier => Il ne peut pas etre ajouter à nouveau
            $( "#value_champ_" + parent_champ_name).val( dossier_module_ids.replace( id_to_delete, '') + "|" + module_id );  
            $( "#display_value_champ_" + champ_name ).empty();
            $( "#display_value_champ_"+ champ_name).attr( "data-id-selected", module_id )
            $( "#display_value_champ_" + champ_name ).append( formatString(module_name) );
        }
        
    }

}
function getDivErreur(){
    var div_erreur = document.createElement("input"); 
    div_erreur.setAttribute("class", "ode_input_hidden");
    div_erreur.setAttribute("value", "Erreur");
    div_erreur.setAttribute("class", "col-md-8");
    div_erreur.setAttribute("disabled", "");
    return div_erreur;
}

function getListes(){
    var input_value = getInputValue("listes_dossier");
    return ( isStringValid (input_value) ) ? isReponseValid( b64_to_utf8(input_value) ) : false;
}

function getInputChamp(champ){
    var div_input = document.createElement("input"); 
    div_input.setAttribute("id", "value_champ_"+champ.name);
    div_input.setAttribute("style", "display: block;background: red !important;");
    div_input.setAttribute("class", "ode_input_hidden col-md-8");
    div_input.setAttribute("value", "value");
    return div_input;
}


function genereBoutonOnglet(parent_id , onglet, count, total){

    var height = ( (100 - total) / total );
    var class_selected = ( count == 0 ) ? "btn_onglet_selected" : "btn_onglet_not_selected";
    var attribut_selected = ( count == 0 ) ? "true" : "false";
    var onglet_id = onglet.id;
    var onglet_libelle = onglet.libelle;
    var onglet_name = onglet.name;
    var onglet_cle = onglet.cle;
    var onglet_champs = onglet.champs;

    var div_container = document.createElement("div"); 
    div_container.setAttribute("class", "div_button_onglet");
    div_container.setAttribute("title", onglet_libelle);
    document.getElementById(parent_id).appendChild(div_container);

    var div_bouton = document.createElement("div"); 
    div_bouton.setAttribute("id", "btn_onglet_" + onglet_id);
    div_bouton.setAttribute("onclick", "displayOnglet(event)");
    div_bouton.setAttribute("class", "btn_onglet "+class_selected);
    div_bouton.setAttribute("is_onglet_selected", attribut_selected);
    div_container.appendChild(div_bouton);

    var div_titre = document.createElement("div"); 
    div_titre.setAttribute("id", "titre_onglet_" + onglet_id);
    div_titre.setAttribute("name", onglet_libelle);
    div_titre.setAttribute("cle", onglet_cle);
    div_titre.setAttribute("class", "div_libelle_button_onglet");
    div_titre.appendChild(document.createTextNode(onglet_libelle.replace(/&#039;/gi, "'")));
    div_bouton.appendChild(div_titre);

}

function saveDossier(){
    execute("addDossier",'OPS_dossier');
}

function getDataOnglets(){
    var data_onglets = {};
    var list_layout = getListLayoutOnglets();
    if( list_layout.length >0 ){
        Object.keys(list_layout).forEach(function(key) { 
            var id = list_layout[key].id;
            if (isStringValid(id)) {
                data_onglets[id] = getDataLayout(id);
            }
        });
    }
    return data_onglets;
}

function getListLayoutOnglets(){

    var list_layouts = [];
    var groupe_btn_onglets = document.getElementById("div_layouts_onglets");
    if(groupe_btn_onglets.getElementsByTagName("div").length > 1){
        var divs = groupe_btn_onglets.getElementsByTagName("div");
        Object.keys(divs).forEach(function(key) {
            var id = divs[key].id;
            if (isStringValid(id)) {
                if(id.indexOf('layout_onglet_') > -1 ){
                    list_layouts.push({
                        id: id,
                    });
                }
            }
        });
    }
    return list_layouts;
}

function getDataLayout(id){
    var data_tbody = {};
    if (isStringValid(id)) {
        if(document.getElementById(id)){
            if( document.getElementById(id).childNodes.length > 0 ){
                var lignes = document.getElementById(id).childNodes;
                Object.keys(lignes).forEach(function(key) { 
                    var ligne = lignes[key];
                    if( ligne.childNodes.length == 2 ){
                        data_tbody[key] = {
                                1: getDataChamp(ligne.childNodes[0].id),
                                2: getDataChamp(ligne.childNodes[1].id)
                        };
                    }
                });
                
            }
        }
    }
    return data_tbody;
}

function getDataChamp(champ_id){

    var data_td = {
        id:"",
        value:""
    };
    var id = "value_"+champ_id;
    if (isStringValid(id)) {
        if(document.getElementById(id)){
            if ( document.getElementById(id).type == "checkbox" ){
                var value = ( document.getElementById(id).checked ) ? "1" : "0";
            } else if( document.getElementById(id).type == "select-multiple" ) {
                var value = getValueListeChoixMulti(id); 
            }else {
                var value = document.getElementById(id).value;
            }
            data_td = {
                id:id.replace(/value_champ_/g,''),
                value:value
            }
        }
    }
    return data_td;
}

function getValueListeChoixMulti(id) {
    if ( $('#'+id).val() ) {
        var value_select_filtred = $('#'+id).val().filter(function (el) { 
            return ( el !== null && el !== undefined && el !== "") ? el : false;
        }); 
    }
    return ( value_select_filtred !== undefined && value_select_filtred.length > 0 ) ? "^" + value_select_filtred.join('^,^') + "^" : "";
}

function formatString(string){
    var string_formated = "";
    if(isStringValid(string)){
       string_formated = string.replace(/&#039;/gi, "'");
    }
    return string_formated;
}

function displayOnglet(event){
    var id = event.target.id;
    if(isStringValid(id) === false){
      if(isStringValid(event.target.parentNode.id)){
          id = event.target.parentNode.id;
      }
    }
    if(isStringValid(id)){
        id = ( id.indexOf('titre_onglet_') > -1 ) ? id.replace(/titre_/g,'btn_') : id;
        selectBtnOnglet(id);
        updateLayoutOnglets();
    }
}

function selectBtnOnglet(id_btn){
 
    var list_btn = getListBtnOnglets();
    if( isStringValid(id_btn) && list_btn.length > 0){
        Object.keys(list_btn).forEach(function(key) {
            if(list_btn[key].id == id_btn){
                setElementAttribute( list_btn[key].id, "is_onglet_selected", "true");
            }else{
                setElementAttribute( list_btn[key].id, "is_onglet_selected", "false");
            }
        });
    }
}

// Fonction qui vérifie l'éxistance de l'élement et ajoute l'attribut
function setElementAttribute( id, attribut_name, attribut_value){
    if( isStringValid(id) && isStringValid(attribut_name) ){
        if( document.getElementById(id) ){
            document.getElementById(id).setAttribute(attribut_name,attribut_value);
        }
    }
}

function getListBtnOnglets(){

    var list_boutons = [];
    var groupe_btn_onglets = document.getElementById("div_boutons_onglets");
    if(groupe_btn_onglets.getElementsByTagName("div").length > 1){
        var divs = groupe_btn_onglets.getElementsByTagName("div");
        Object.keys(divs).forEach(function(key) {
            var div_id = divs[key].id;
            var div = divs[key];
            if( div_id.indexOf('btn_onglet_') > -1 ){
                list_boutons.push({
                    id: div_id,
                    is_onglet_selected: div.getAttribute("is_onglet_selected")
                });
            }
        });
    }
    
    return list_boutons;
}

function updateLayoutOnglets(){
    var list_btn = getListBtnOnglets();
    if( list_btn.length > 0 ){
        Object.keys(list_btn).forEach(function(key) { 
            var id = list_btn[key].id;
            var is_selected = list_btn[key].is_onglet_selected;
            if ( id.indexOf('btn_onglet_') > -1 ){
                var name = id.replace(/btn_onglet_/g,'');
                if(is_selected == "true"){
                    changeElementDisplay("layout_onglet_"+name, "block");
                    changeElementClass(id, "btn_onglet btn_onglet_selected");
                }else{
                    changeElementDisplay("layout_onglet_"+name, "none");
                    changeElementClass(id, "btn_onglet btn_onglet_not_selected");
                }
            }
        });
    }
}

/**------------------------------------------------------------------------- Fonctions Ajax ------------------------------------------------------------------- */

// Cette variable regroupe les differentes fonctions des actions à mener apres la réponse de l'Ajax
var retourFunctionCreate = {
    initForm: function(data) {
        //ODE_LOADING.removeSpinner($(document.body));
        if (data) {
            if (data['statut'] == "ok") {
                displayDossierForm(data['data'] );
                if( $_GET("id_tiers") != null &&  $_GET("type_tiers") != null ){
                    document.getElementById('display_value_champ_demandeur').innerHTML = String( $_GET("tiers") ).b64_to_utf8();
                    document.getElementById('value_champ_demandeur').value = $_GET("id_tiers");
                }

            } else {
                var erreur = data['data']; 
                console.log("addOnglet() => Erreur =", erreur)
            }
        } else {
            console.log(" addOnglet() => format de retour différent de JSON ")
        }
      //  ODE_LOADING.removeSpinner($(document.body));
    },
    addDossier: function(data) {

        if (data) {
            if (data['statut'] == "ok") {
               var redirect = "/index.php?module=OPS_dossier&return_module=OPS_dossier&action=DetailView&record="+data['data']['id'];
               window.location.replace(redirect);
            } else {
                ODE_LOADING.removeSpinner($(document.body));
                removeElementAttribute( "btn_sauvegarder_generateur_vue", "disabled")
                var erreur = data['data']; 
                
                console.log("addDossier() => Erreur =", erreur)
            }
        } else {
            ODE_LOADING.removeSpinner($(document.body));
            removeElementAttribute( "btn_sauvegarder_generateur_vue", "disabled")
            console.log(" addDossier() => format de retour différent de JSON ")
        }
        
    },
};
var retourFunction = (typeof retourFunction === 'undefined') ? retourFunctionCreate : Object.assign(retourFunction, retourFunctionCreate);

// Cette variable regroupe les differentes fonctions des actions en attendant de recevoir le retour de la requete à envoyer au PHP
var loadingFunctionCreate = {
    initForm: function() {
       //ODE_LOADING.addSpinner($(document.body));
    },
    addDossier: function() {
        ODE_LOADING.addSpinner($(document.body));
    },
};
var loadingFunction = (typeof loadingFunction === 'undefined') ? loadingFunctionCreate : Object.assign(loadingFunction, loadingFunctionCreate);

// Cette variable regroupe les differentes fonctions de récupération des données à envoyer au PHP
var getDataFunctionCreate = {
    initForm: function() {
        var dispositif_id =  getInputValue("dispositif_choisi_id");
        var demandeur_type =  getInputValue("demandeur_type_choisi");
        var data = (isStringValid(dispositif_id) && isStringValid(demandeur_type)) ? "dispositif_id=" + dispositif_id + "&demandeur_type=" + demandeur_type : false;
        return data;
    },
    addDossier: function() {
        $(".div_button_onglet" ).removeClass('tab-error');
        setElementAttribute( "btn_sauvegarder_generateur_vue", "disabled", "true")
        document.getElementById("btn_sauvegarder_generateur_vue").style.backgroundColor = "#f08377";
        var dispositif_id =  getInputValue("dispositif_choisi_id");
        var demandeur_type =  getInputValue("demandeur_type_choisi");
        var type_tiers = ( demandeur_type == "OPS_individu") ? "Individu" : "Personne Morale";
        var data_formulaire = getDataOnglets();
        var json = ( verificationDataFormulaire(data_formulaire) === true) ? JSON.stringify(data_formulaire) : false;
        var data = ( isJson(json) && isStringValid(dispositif_id) && isStringValid(type_tiers)) ? "json=" + utf8_to_b64(json) +"&dispositif_id=" + dispositif_id + "&type_tiers=" + type_tiers : false;
        if( data === false ) removeElementAttribute( "btn_sauvegarder_generateur_vue", "disabled")
        return data;
    },
};
var getDataFunction = (typeof getDataFunction === 'undefined') ? getDataFunctionCreate : Object.assign(getDataFunction, getDataFunctionCreate);

function verificationDataFormulaire(data_formulaire){
    var create = true;
    if( Object.size(data_formulaire) > 0 ){
        Object.keys(data_formulaire).forEach(function(onglet_key) {
            var onglet = data_formulaire[onglet_key];
            if( Object.size(onglet) > 0 ){
                Object.keys(onglet).forEach(function(ligne_key) {
                    var ligne = onglet[ligne_key];
                    if( Object.size(ligne) > 0 ){
                        Object.keys(ligne).forEach(function(champ_key) {
                            var champ = ligne[champ_key];
                            var champ_id = ( isStringValid(champ.id) ) ? champ.id : false;
                            var element = ( champ_id !== false ) ? document.getElementById("value_champ_"+champ_id) : false;
                            if( element !== false ){
                                var is_valid = OV.isValid(element);
                                if( is_valid === false ) create = false;
                            }
                        });
                    }
                });
            }
        });
    }else{
        create = false;
        console.log(" Formulaire vide ! ")
    }

    return create;
}

function isPourcentageValid(_this){
    var value = (_this.value) ? _this.value : 0;
    if( value > 100 ) _this.value = 100 ;
    if( value < 0 ) _this.value = 0 ;
}
