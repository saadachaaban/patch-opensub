<?php

class OdeListeChoixMulti
{

    var $editable;

    /**
     * @access public
     * @name __construct
     * @param string			$name: Le nom du champ à afficher
     * @param string			$value: La valeur en BDD
     * @param string			$params: Les parametres du champ
     * @param boolean			$editable: Est ce que le champ est éditable ?
     * 
     * @return void
     */
    function __construct($editable = false)
    {
        $this->editable = $editable;
    }

    /**
     * @access public
     * @name getHtml()
     * Fonction qui renvoie le code HTML à afficher 
     *
     *  @return string          $html: Le champ ( Format html )
     */
    public function getHtml($champ, $value)
    {
        return ($this->editable === false) ? $this->getIneditableField($champ, $value) : $this->getEditableField($champ, $value);
    }

    /**
     * @access private
     * @name getIneditableField()
     * Fonction qui renvoie le code HTML à afficher dans la vue détail
     *
     *  @return string          $html: Le champ ( Format html )
     */
    private function getIneditableField($champ, $value_bdd)
    {
        $value = $this->getValueField($value_bdd);
        $liste = $this->getListe($champ["params"]);
        if (!empty($champ["name"])) {
            $html = '<div class="col-xs-12 col-sm-8 detail-view-field " style="width: 70%; margin-left: 5%;" type="multienum" field="' . $champ["name"] . '">';
            foreach ($value as $value_key) {
                if (array_key_exists($value_key, $liste)) {
                    $html .= '<li style="margin-left:10px;">' . htmlspecialchars($liste[$value_key], ENT_QUOTES)  . '</li>';
                }
            }
            $html .= '</div>';
        } else {
            $html = $this->getErreurField("Erreur d'initialisation");
        }
        return $html;
    }

    /**
     * @access private
     * @name getListe()
     * Fonction qui retourne la liste de valeurs du champs
     *
     *  @return array          $liste: liste en tableau ou tableau vide
     */
    private function getListe($params)
    {
        $liste = array();
        if ($params !== null && !empty($params)) {
            $params_utf8 = base64_decode($params);
            $params_array = json_decode($params_utf8, true);
            if (is_array($params_array) && count($params_array) > 0) {
                if (!empty($params_array['liste'])) {
                    $liste = $params_array['liste'];
                }
            }
        }
        return $liste;
    }

    /**
     * @access private
     * @name getEditableField()
     * Fonction qui renvoie le code HTML à afficher dans la vue édit
     *
     *  @return string          $html: Le champ ( Format html )
     */
    private function getEditableField($champ, $value_bdd)
    {
        $value = $this->getValueField($value_bdd);
        $liste = $this->getListe($champ["params"]);
        /*
        $GLOBALS['log']->fatal(" OdeListeChoixMulti :: getEditableField() =>  " . $champ['name'] . print_r(array(
            "liste" => $liste,
            "value" => $value,
        ), true));
        */
        if (!empty($champ["name"])) {
            $html = '<select class="col-md-8" id="value_champ_' . $champ["name"] . '" multiple="">';
            foreach ($liste as $key => $value_liste) {
                if (in_array($key, $value)) {
                    $html .= '<option value="' . $key . '" selected >' . $value_liste . '</option>';
                } else {
                    $html .= '<option value="' . $key . '" >' . $value_liste . '</option>';
                }
            }
            $html .= '</select>';
        } else {
            $html = $this->getErreurField("Erreur d'initialisation");
        }
        return $html;
    }

    /**
     * @access private
     * @name getValueField()
     * Fonction qui retourne la valeur à afficher du champ
     *
     *  @return array           $value: les valeurs du champ ou tableau vide
     */
    private function getValueField($value_bdd)
    {
        $value = array();
        do {
            // Si la valeur est vide
            if ($value_bdd === null || empty($value_bdd)) {
                break;
            }
            // On vérifie que c'est bien la valeur d'une liste choix multi
            if (strpos($value_bdd, "^") === false) {
                break;
            }
            // On supprime le premier et le dernier caractère   [  ^value^ => value  |   ^value_1^,^value_2^ => value_1^,^value_2  ]  
            $value_formated = substr(substr($value_bdd, 1), 0, -1);
            $value = explode("^,^", $value_formated);
        } while (0);
        return $value;
    }

    /**
     * @access private
     * @name getErreurField()
     *
     *  @param string			$message: Le message d'erreur
     *  @return string          $html: champ de type input avec le message d'erreur
     */
    private function getErreurField($message = "")
    {
        $html = '<input type="text" value="' . $message . '" style="display: block;color: #f08377;background: #f8f8f8 !important;border: 1px solid red;" class="ode_input_hidden col-md-8">';
        return $html;
    }
}
