<?php

require_once "custom/include/ode_generateur/fields/OdeListeChoixMulti.php";

// Récupére la valeur du champs => formatage => getField 
class OdeFieldFactory
{

    var $factory;
    var $editable;
    var $fields_folder_route = "custom/include/ode_generateur/fields";

    function __construct($editable = false)
    {
        $this->editable = $editable;
        $this->default_html = "Default html ";
        $this->factory = $this->initFieldFactory();
    }

    function initFieldFactory()
    {
        $factory = array();
        if (is_dir($this->fields_folder_route)) {
            $fields_folder = scandir($this->fields_folder_route);
            foreach ($fields_folder as $key => $file_name) {
                if ($file_name != "." && $file_name != "..") {
                    $class_name = str_replace('.php', '', $file_name);
                    $factory[strtolower(str_replace('Ode', '', $class_name))] = new $class_name($this->editable);
                }
            }
        } else {
            echo "Erreur chemin";
        }
        return $factory;
    }

    function getHtml($champ, $value)
    {

        /*
            echo " Value = " . $value;

            echo "<pre>";
            print_r($champ);
            echo "</pre>";

            echo "<pre>";
            print_r($this->factory);
            echo "</pre>";
        */

        $html = "";
        $type = strtolower(str_replace("_", "", $champ["type"]));
        if (array_key_exists($type, $this->factory)) {
            $html = $this->factory[$type]->getHtml($champ, $value);
        }
        return (!empty($html)) ? $html : $this->default_html;
    }
}
