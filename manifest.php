 <?php

    $manifest = [
        0 => [
            'acceptable_sugar_versions' => [
                0 => ''
            ]
        ],
        1 => [
            'acceptable_sugar_flavors' => [
                0 => 'CE',
                1 => 'PRO',
                2 => 'ENT'
            ]
        ],
        'readme' => '',
        'key' => 'OPS',
        'author' => 'ODE PROJECT',
        'description' => 'Open Socle est le core des applications ODE Project. Ce patch corrige les anomalies de la version 2.1',
        'icon' => '',
        'is_uninstallable' => true,
        'name' => 'ODE - CORE Patch 2.1',
        'published_date' => '2021-08-26 10:00:00',
        'type' => 'module',
        'version' => '1.0',
        'release' => '01',
        'sticky'  => '598',
        'remove_tables' => 'false'
    ];

    $installdefs = [

        'id' => 'ode_core_patch',

        'copy' => [
            [
                'from' => '<basepath>/modules/OPS_dossier/views/view.detail.php',
                'to' => 'modules/OPS_dossier/views/view.detail.php'
            ],
            [
                'from' => '<basepath>/custom/include/ode_generateur',
                'to' => 'custom/include/ode_generateur'
            ],
            [
                'from' => '<basepath>/modules/OPS_dossier/views/view.modifier.php',
                'to' => 'modules/OPS_dossier/views/view.modifier.php'
            ],
            [
                'from' => '<basepath>/modules/OPS_dossier/js/create.js',
                'to' => 'modules/OPS_dossier/js/create.js'
            ],
            [
                'from' => '<basepath>/modules/OPS_dossier/js/modifier.js',
                'to' => 'modules/OPS_dossier/js/modifier.js'
            ],
        ],

    ];
